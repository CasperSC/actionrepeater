﻿using System.Diagnostics;

namespace WpfApp1
{
    [DebuggerDisplay("Url = {Url}")]
    public class Data
    {
        public Data()
        {
        }

        public Data(string url)
        {
            Url = url;
        }

        public string Url { get; set; }
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        ActionRepeater<Data> _repeater;

        public MainWindow()
        {
            Messages = new ObservableCollection<string>();
            InitializeComponent();

            _repeater = new ActionRepeater<Data>();
            _repeater.Invoked += ActionRepeater_Invoked;

            _repeater.Add(new Data("Адрес 1"));
            _repeater.Add(new Data("Адрес 2"));
            _repeater.Add(new Data("Адрес 3"));
            _repeater.Add(new Data("Адрес 4"));
            _repeater.Add(new Data("Адрес 5"));
            _repeater.Add(new Data("Адрес 6"));
            _repeater.Add(new Data("Адрес 7"));
            _repeater.Add(new Data("Адрес 8"));
        }

        private void ActionRepeater_Invoked(Data data, CancellationToken token)
        {
            // Часть работы, занимающей время

            // Перед ещё более долгой работой проверяем, если запросили отмену, то не выполяем дальнейшие действия.
            if (token.IsCancellationRequested)
            {
                return;
            }

            // Тут отправляй АСИНХРОННЫЕ запросы в веб.

            // Dispatcher.BeginInvoke нужен, чтобы перенаправлять выполенение в UI поток.
            // Имеет смысл вызыввать только при обращении к UI.
            Dispatcher.BeginInvoke(new Action<Data>((item) =>
            {
                Messages.Add(item.Url);
            }), data);
        }

        public ObservableCollection<string> Messages { get; set; }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            _repeater.Start();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            _repeater.Stop();
        }
    }
}

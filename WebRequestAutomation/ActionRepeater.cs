﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace WpfApp1
{
    public delegate void EventInvoker<TData, CancellationToken>(TData data, CancellationToken cancellationToken);

    public class ActionRepeater<TData>
        where TData : class
    {
        private readonly object _startStopAddSync = new object();

        private readonly int _maxParallelism;
        private volatile bool _canWork;
        private volatile int _currentIndex;
        private volatile bool _isStarted;
        private CancellationTokenSource _cts;

        private int _timeout;
        private readonly DataWrapper<TData>[] _data;

        public ActionRepeater(int? maxParallelism = null)
        {
            if (!maxParallelism.HasValue)
            {
                maxParallelism = Environment.ProcessorCount;
            }
            else if (maxParallelism < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(maxParallelism));
            }

            _maxParallelism = maxParallelism.Value;
            _data = new DataWrapper<TData>[maxParallelism.Value];
            for (int i = 0; i < _data.Length; i++)
            {
                _data[i] = new DataWrapper<TData>();
            }

            Timeout = 1000;
        }

        ~ActionRepeater()
        {
            if (_cts != null)
            {
                _cts.Dispose();
            }
        }

        public event EventInvoker<TData, CancellationToken> Invoked;

        public bool IsStarted { get => _isStarted; }

        public int Timeout
        {
            get => _timeout;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }
                _timeout = value;
            }
        }

        public void Add(TData data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            lock (_startStopAddSync)
            {
                _data[_currentIndex].Items.Add(data);

                if (++_currentIndex == _maxParallelism)
                {
                    _currentIndex = 0;
                }
            }
        }

        public void Start()
        {
            if (!_isStarted)
            {
                lock (_startStopAddSync)
                {
                    if (_isStarted)
                        return;

                    _cts = new CancellationTokenSource();
                    Array.ForEach(_data, (item) => item.Reset());

                    _canWork = true;
                    _isStarted = true;
                }

                Execute();
            }
        }

        public void Stop()
        {
            if (_isStarted)
            {
                lock (_startStopAddSync)
                {
                    if (!_isStarted)
                        return;

                    _cts.Cancel();
                    _cts.Dispose();
                    _cts = null;

                    _canWork = false;

                    Array.ForEach(_data, (item) => item.Set());

                    _isStarted = false;
                }
            }
        }

        private void OnInvoked(TData data, CancellationToken token)
        {
            Invoked?.Invoke(data, token);
        }

        private void Execute()
        {
            var taskCount = _maxParallelism;
            var tasks = new Task[taskCount];
            for (int i = 0; i < taskCount; i++)
            {
                DataWrapper<TData> wrapper = _data[i];

                tasks[i] = Task.Factory.StartNew(() =>
                {
                    ConcurrentBag<TData> bag = wrapper.Items;
                    var temp = new List<TData>(bag.Count);
                    while (_canWork)
                    {
                        while (_canWork && !bag.IsEmpty)
                        {
                            TData item = null;
                            while (!bag.TryTake(out item))
                            {
                            }
                            temp.Add(item);

                            if (item != null)
                            {
                                try
                                {
                                    if (_cts.IsCancellationRequested)
                                    {
                                        throw new OperationCanceledException();
                                    }

                                    OnInvoked(item, _cts.Token);
                                }
                                catch
                                {
                                    continue;
                                }
                            }
                        }

                        temp.ForEach(value => bag.Add(value));
                        temp.Clear();

                        if (Timeout > 0)
                        {
                            wrapper.Wait(Timeout);
                        }
                    }
                });
            }
        }

        private class DataWrapper<T> where T : TData
        {
            public ManualResetEventSlim PauseEvent { get; } = new ManualResetEventSlim(true);
            public ConcurrentBag<TData> Items { get; }

            public DataWrapper()
            {
                Items = new ConcurrentBag<TData>();
            }

            ~DataWrapper()
            {
                PauseEvent.Dispose();
            }

            public void Set()
            {
                PauseEvent.Set();
            }

            public void Reset()
            {
                PauseEvent.Reset();
            }

            internal void Wait(int timeout)
            {
                PauseEvent.Wait(timeout);
            }
        }
    }
}
